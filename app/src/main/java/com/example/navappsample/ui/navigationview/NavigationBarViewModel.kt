package com.example.navappsample.ui.navigationview

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel

class NavigationBarViewModel:ViewModel() {
    private val _items = mutableStateListOf<String>()
    val items:List<String> = _items
    init {
        getItems()
    }
    fun getItems(){
        repeat(20){
            _items.add(it.toString())
        }
    }

    fun updateItem(index:Int){
        _items[index] = "Clicked"
    }
}