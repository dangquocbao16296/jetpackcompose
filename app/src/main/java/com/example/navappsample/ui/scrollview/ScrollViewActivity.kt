package com.example.navappsample.ui.scrollview

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.navappsample.common.base.BaseActivity
import com.example.navappsample.common.base.setupAlerDialog
import com.example.navappsample.ui.theme.NavAppSampleTheme

class ScrollViewActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NavAppSampleTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    SrollViewScreen()
                }
            }
        }
    }
    @Preview(showBackground = true)
    @Composable
    fun DefaultPreview4() {
        NavAppSampleTheme {
            SrollViewScreen()
        }
    }
}

@Composable
fun Greeting7(name: String) {
    Text(text = "Hello $name!")
}

