package com.example.navappsample.ui.loadmorelist

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color

data class ItemList(
    var title:String,
    var description:String,
    var isLoading:Boolean,
    var isSelected:Boolean = false,
    var test:MutableState<String> = mutableStateOf(""),
    var color: MutableState<Color> = mutableStateOf(Color.DarkGray)
)
