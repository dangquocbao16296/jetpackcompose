package com.example.navappsample.ui.customview

import android.util.Log
import android.widget.TextView
import androidx.annotation.Dimension
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.focus.onFocusEvent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.*
import androidx.compose.ui.viewinterop.AndroidView
import androidx.constraintlayout.compose.Dimension.Companion.fillToConstraints
import com.example.navappsample.common.base.*
import com.example.navappsample.common.extension.findActivity
import com.example.navappsample.common.extension.type12
import com.example.navappsample.ui.theme.NavAppSampleTheme


@Preview(showBackground = true)
@Composable
fun DefaultPreview5() {
    NavAppSampleTheme {
        CustomViewScreen()
    }
}

@Composable
fun CustomViewScreen(){
    Column(modifier = Modifier.fillMaxSize()) {
        MyHeader("Custom view")
        RegularTextView()
    }
}

@Composable
fun RegularTextView(){
    var string = remember {
        mutableStateOf("RegularTextView")
    }
    val focusManager =  LocalFocusManager.current
    Column(horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .clickable {
                focusManager.clearFocus()
            }
            .verticalScroll(
                rememberScrollState()
            )
    ) {
        AndroidView(
            factory = { context ->
//                TextView(context).apply {
//                    textSize = 34.dp.value
//                }
                var view = TextView(context)
                view.type12()
                view.includeFontPadding = false
                view
            },
            update = { textView ->
                textView.text = string.value
            }
        )
        Divider(color = Color.LightGray, thickness = 1.dp)
        AndroidView(
            factory = { context ->
//                TextView(context).apply {
//                    textSize = 34.dp.value
//                }
                var view = TextView(context)
                view.type12()
                view.includeFontPadding = false
                view.text = string.value
                view
            },
            update = { textView ->
                textView.text = string.value
            }
        )
        Spacer(modifier = Modifier.height(0.dp))
        Button(
            onClick = {
                string.value = "Button clicked"
            },
        ) {
            Text(text = "Update text")
        }
        Divider(color = Color.LightGray, thickness = 1.dp)
        Text("Hi there!", Modifier.firstBaselineToTop(15.dp))
        Divider(color = Color.LightGray, thickness = 1.dp)
        Text("Hi there!", Modifier.padding(top = 0.dp))
        Divider(color = Color.LightGray, thickness = 1.dp)
        MyText(string.value)
        MyText(string.value, color = Color.Blue)
        MyText(string.value, color = Color.Blue)
        CallingComposable()
        MyText(string.value, color = Color.Red)
        MyButton(modifier = Modifier.fillMaxWidth()) {
            Text(text = "click me")
        }
        BorderStrolkeButton(modifier = Modifier
            .fillMaxWidth()
            .padding(start = 5.dp, end = 5.dp)
        ) {
            Text(text = "click me")
        }

        ConstraintLayout()
        ConstraintLayout()
        ConstraintLayout()
        TestCustomTextField()
        LabelAndPlaceHolder()
        BackGroundTextField()
        CustomBasicTextField()
        val extra = LocalContext.current.findActivity()?.intent?.getStringExtra("extra")
        Text(text = extra ?: "")
    }
}

@Composable
fun TestCustomTextField(){
    var value = remember {
        mutableStateOf("")
    }
    var lable = remember {
        mutableStateOf("Enter some text")
    }
    var errMess = remember {
        mutableStateOf("")
    }
    var isError = remember {
        mutableStateOf(false)
    }
    var isFirst = remember {
        mutableStateOf(true)
    }
    MyOutlinedTextField(value = value.value,
        label = lable.value,
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 5.dp, end = 5.dp)
            .onFocusChanged {
//                Log.d("BAO", "-------> is captured: ${it.isCaptured}")
                Log.d("BAO", "-----> isFocused: ${it.isFocused}")
                if (!it.isFocused) {
                    if (value.value.isNullOrEmpty()) {
                        if (!isFirst.value) {
                            errMess.value = "Can't empty on here!"
                            isError.value = true
                        } else {
                            isFirst.value = false
                        }
                    }
                }
            },
        errorMessage = errMess.value,
        isError = isError.value,
        onValueChanged = {
            value.value = it ?: ""
        },
        onClickErrorIcon = {
            errMess.value = ""
            isError.value = false
            value.value = ""
        }
    )
}


fun Modifier.firstBaselineToTop(
    firstBaselineToTop: Dp
) = layout { measurable, constraints ->
    // Measure the composable
    val placeable = measurable.measure(constraints)

    // Check the composable has a first baseline
    check(placeable[FirstBaseline] != AlignmentLine.Unspecified)
    val firstBaseline = placeable[FirstBaseline]

    // Height of the composable with padding - first baseline
    val placeableY = firstBaselineToTop.roundToPx() - firstBaseline
    val height = placeable.height + placeableY
    layout(placeable.width, height) {
        // Where the composable gets placed
        placeable.placeRelative(0, placeableY)
    }
}

@Composable
fun MyText(value:String? = null, modifier: Modifier = Modifier
           , color: Color = Color.DarkGray
){
    if (value.isNullOrEmpty()){

    }else{
        Text(text = value,
            fontSize = 12.sp,
            modifier = modifier,
            color = color,
            overflow = TextOverflow.Clip
        )
    }

}

fun Modifier.baselinePadding(
    firstBaselineToTop: Dp,
    lastBaselineToBottom: Dp
) = layout { measurable, constraints ->
    val placeable = measurable.measure(constraints)

    check(placeable[FirstBaseline] != AlignmentLine.Unspecified)
    val firstBaseline = placeable[FirstBaseline]

    check(placeable[LastBaseline] != AlignmentLine.Unspecified)
    val lastBaseline = placeable[LastBaseline]

    val lastBaselineToBottomHeight = placeable.height - lastBaseline

    val lastBaselineToBottomDelta = lastBaselineToBottom.roundToPx() - lastBaselineToBottomHeight

    val totalHeight = placeable.height +
            (firstBaselineToTop.roundToPx() - firstBaseline)

    val placeableY = totalHeight - placeable.height
    layout(placeable.width, totalHeight + lastBaselineToBottomDelta) {
        placeable.placeRelative(0, placeableY)
    }
}

@Composable
fun MyBasicColumn(
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    Layout(
        modifier = modifier,
        content = content
    ) { measurables, constraints ->
        // Don't constrain child views further, measure them with given constraints
        // List of measured children
        val placeables = measurables.map { measurable ->
            // Measure each children
            measurable.measure(constraints)
        }

        // Set the size of the layout as big as it can
        layout(constraints.minWidth, constraints.minHeight) {
            // Track the y co-ord we have placed children up to
            var yPosition = 0

            // Place children in the parent layout
            placeables.forEach { placeable ->
                // Position item on the screen
                placeable.placeRelative(x = 0, y = yPosition)

                // Record the y co-ord placed up to
                yPosition += placeable.height
            }
        }
    }
}

@Composable
fun CallingComposable(modifier: Modifier = Modifier) {
    MyBasicColumn(
        modifier
            .padding(8.dp)
            .fillMaxWidth()) {
        Text("MyBasicColumn")
        Text("places items")
        Text("vertically.")
        Text("We've done it by hand! We've " +
                "done it by hand! We've done it by hand!" +
                "We've done it by hand!We've done it by hand!We've done it by hand!"
        )
    }
}

@Composable
fun ConstraintLayout(){
    Column() {
        androidx.constraintlayout.compose.ConstraintLayout(modifier = Modifier.fillMaxWidth()) {
            val (text, button, line) = createRefs()
            Text(
                text = "value",
                // Assign reference "button" to the Button composable
                // and constrain it to the top of the ConstraintLayout
                modifier = Modifier.constrainAs(button) {
                    top.linkTo(parent.top, margin = 0.dp)
                    end.linkTo(parent.end, margin = 16.dp)
                },
                fontWeight = FontWeight.Bold
            )

            // Assign reference "text" to the Text composable
            // and constrain it to the bottom of the Button composable
            Text(
                text="testtesttestt" +
                        "esttest test test" +
                        " test  test  test  test  test  test " +
                        " test  test  test  test  test  test ",
                Modifier.constrainAs(text) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(button.start, margin = 10.dp)
                    width = fillToConstraints
                })
        }
        Spacer(modifier = Modifier.height(5.dp))
        Divider(color = Color.LightGray, thickness = 1.dp)
    }

}