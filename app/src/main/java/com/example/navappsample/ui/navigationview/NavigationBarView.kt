package com.example.navappsample.ui.navigationview

import android.annotation.SuppressLint
import android.util.Log
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import coil.compose.SubcomposeAsyncImage
import com.example.navappsample.common.NavItemModel
import com.example.navappsample.common.Routes
import com.example.navappsample.common.RoutesNav
import com.example.navappsample.common.ViewModelFactory
import com.example.navappsample.common.base.MyHeader
import com.example.navappsample.ui.customview.RegularTextView
import com.example.navappsample.ui.loadmorelist.DynamicListViewModel
import com.example.navappsample.ui.ui.theme.Purple200

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun NavigationBarScreen(){
    val navController = rememberNavController()
//    Column(modifier = Modifier.fillMaxSize()) {
////        MyHeader("Navigation Bar")
//        Scaffold(modifier = Modifier.fillMaxSize(),
//            bottomBar = {BottomNavBar(navController)}
//        ) {
//            Box(modifier = Modifier
//                .padding(it)
//            )
//            NavigationHost(navController = navController)
//        }
//    }
    Scaffold(modifier = Modifier.fillMaxSize(),
        scaffoldState = rememberScaffoldState(),
        bottomBar = {BottomNavBar(navController)},
        topBar = { MyHeader("Navigation Bar")}
    ) { innerPadding ->
        Box(modifier = Modifier.padding(innerPadding)) {
            NavigationHost(navController = navController)
        }

//        NavigationHost(navController = navController)
    }
}

@Composable
fun BottomNavBar(navController: NavHostController){
    BottomNavigation(backgroundColor = Purple200,
        modifier = Modifier.clip(RoundedCornerShape(15.dp, 15.dp, 0.dp, 0.dp)),
        elevation = 5.dp
    ) {
        val backStackEntry by navController.currentBackStackEntryAsState()
        val currentRoute = backStackEntry?.destination?.route
        val navItems = listOf(
            NavItemModel(
                title = "Home",
                icon = Icons.Filled.Home,
                route = "Home"
            ),
            NavItemModel(
                title = "Contact",
                icon = Icons.Filled.Face,
                route = "Contact"
            ),
            NavItemModel(
                title = "Setting",
                icon = Icons.Filled.Settings,
                route = "Setting"
            )
        )
        //
        navItems.forEachIndexed { index, navItemModel ->
            BottomNavigationItem(
                selected = currentRoute == navItemModel.route,
                onClick = {
                    navController.navigate(navItemModel.route) {
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        launchSingleTop = true
                        restoreState = true
                    }
                },

                icon = {
                    Icon(imageVector = navItemModel.icon,
                        contentDescription = navItemModel.title)
                },
                label = {
                    Text(text = navItemModel.title)
                },
                selectedContentColor = Color.Blue,
                unselectedContentColor = Color.DarkGray,
            )
        }
    }
}

@Composable
fun NavigationHost(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = RoutesNav.Home.route,
    ) {
        composable(RoutesNav.Home.route) {
            EmptyScreen(RoutesNav.Home.route, viewModel<NavigationBarViewModel>())
        }

        composable(RoutesNav.Contact.route) {
            EmptyScreen1(viewModel<EmptyViewModel1>())
        }

        composable(RoutesNav.Setting.route) {
            EmptyScreen2()
        }
    }
}

@Composable
fun EmptyScreen(title:String = "", viewModel: NavigationBarViewModel){
//    val viewModel = remember {
//        ViewModelFactory().create(NavigationBarViewModel::class.java)
//    }

//    val list = mutableListOf<String>()
//    repeat(20){
//        list.add(it.toString())
//    }
    val titleState = remember {
        mutableStateOf(title)
    }
//    val items = remember {
//        mutableStateOf(list)
//    }
    Log.d("BAO", "------> Create EmptyScreen")
    Column(Modifier.fillMaxSize()) {
        Log.d("BAO", "------> Create EmptyScreen1")


        Text(text = titleState.value,
            modifier = Modifier.align(Alignment.CenterHorizontally),
            textAlign = TextAlign.Center
        )
        LazyColumn(contentPadding = PaddingValues(5.dp)){

            itemsIndexed(viewModel.items){index, item ->
                Log.d("BAO", "-------> create item: ${item}")
                Column(Modifier.fillMaxWidth()) {
                    SubcomposeAsyncImage(
                        model = "https://reqres.in/img/faces/1-image.jpg",
                        loading = {
                            CircularProgressIndicator()
                        },
                        contentDescription = "",
                        modifier = Modifier
                            .size(70.dp)
                            .clickable {
                                viewModel.updateItem(index)
                            }
                    )
                    Text(text = "Text ${item}",
                        modifier = Modifier
                            .fillMaxWidth()
                            .requiredHeight(40.dp)
                            .background(color = Purple200, shape = RoundedCornerShape(5.dp))
                            .align(Alignment.CenterHorizontally)
                    )

                }


                Spacer(Modifier.height(5.dp))
            }
//            repeat(20){
//                item() {
//                    Log.d("BAO", "-------> create item: ${it}")
//                    Text(text = "Text ${it}",
//                        modifier = Modifier
//                            .fillMaxWidth()
//                            .requiredHeight(40.dp)
//                            .background(color = Purple200, shape = RoundedCornerShape(5.dp))
//                            .align(Alignment.CenterHorizontally)
//                    )
//                    Spacer(Modifier.height(5.dp))
//                }
//
//            }
        }
    }
}

@Composable
fun EmptyScreen1(viewModel:EmptyViewModel1){
//    var value = remember {
//        mutableStateOf("Empty screen 1")
//    }
    Log.d("BAO", "-----> create empty screen 1")
    Scaffold(Modifier.fillMaxSize(), scaffoldState = rememberScaffoldState()) {
        Column(Modifier.padding(it)) {
            Text(text = viewModel.text.value)
            Button(onClick = { viewModel.updateText()}) {
                Text(text = "click")
            }
        }
    }
}

@Composable
fun EmptyScreen2(){
    var value = remember {
        mutableStateOf("Empty screen 2")
    }
    Scaffold(Modifier.fillMaxSize(), scaffoldState = rememberScaffoldState()) {
        Column(Modifier.padding(it)) {
            Text(text = value.value)
            Button(onClick = { value.value = "clicked" }) {
                Text(text = "click")
            }
        }
    }
}