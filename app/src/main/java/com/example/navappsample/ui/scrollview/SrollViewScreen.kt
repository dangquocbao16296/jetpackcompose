package com.example.navappsample.ui.scrollview

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.interaction.Interaction
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.unit.dp
import com.example.navappsample.common.base.baseHeader
import kotlinx.coroutines.*

@Composable
fun ScrollViewActivity.SrollViewScreen(){
    val scrollSatte = rememberScrollState()
//    runBlocking {
//        launch(Dispatchers.IO) {
//            scrollSatte.scroll {
//
//            }
//        }
//    }
    val nestedScrollConnection = remember {
        object : NestedScrollConnection {
            override fun onPreScroll(available: Offset, source: NestedScrollSource): Offset {
                val delta = available.y
                // called when you scroll the content
                Log.d("BAO", "------> scroll y: $delta")
                return Offset.Zero
            }
        }
    }
    val list = remember {
        mutableStateListOf<String>()
    }
    list.addAll(getList())
    val list1:List<String> = list
    Column(modifier = Modifier
        .fillMaxSize()
        .verticalScroll(scrollSatte)
        .nestedScroll(nestedScrollConnection)
        ) {
        baseHeader(title = "Scroll View")
        for (i in 0..10){
            Box(
                Modifier
                    .fillMaxWidth()
                    .height(200.dp)
                    .background(Color.Green)
            ) {
                Text(text = "Position: $i",
                    modifier = Modifier.align(Alignment.Center)
                )
            }
            Spacer(modifier = Modifier.height(5.dp))
        }
//        val list1 = mutableListOf<String>()

        Log.d("BAO", "----------><------------")

//        for (i in 0..10){
//            list.add(i.toString())
//        }




//        list1.forEachIndexed { index, i ->
//            Button(
//                onClick = {
//                    Log.d("BAO", "------> click : ${index}")
//                    list[index] = "Clicked"
//                },
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .defaultMinSize(minHeight = 50.dp)
//            ) {
//                Text(text = "Position: $i")
//            }
//
//            Spacer(modifier = Modifier.height(10.dp))
//        }

        val listState = rememberLazyListState()

//        LazyColumn(modifier = Modifier
//            .height(200.dp)
//            .padding(start = 10.dp, end = 10.dp)){
//            itemsIndexed(list){ index, item ->
//                Button(
//                    onClick = {
//                        Log.d("BAO", "------> click : ${index}")
//                        list[index] = "Clicked"
//                    },
//                    modifier = Modifier
//                        .fillMaxWidth()
//                        .defaultMinSize(minHeight = 50.dp)
//                ) {
//                    Text(text = "Position: $item")
//                }
//
//                Spacer(modifier = Modifier.height(10.dp))
//            }
//        }

    }
}

fun getList():ArrayList<String>{
    var arr = ArrayList<String>()
    repeat(10){
        arr.add(it.toString())
    }
    return arr
}