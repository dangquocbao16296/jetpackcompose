package com.example.navappsample.ui.dashboard

import android.content.Intent
import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.layout
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.example.navappsample.ui.alertdiog.AlertDialogActivity
import com.example.navappsample.ui.animation.AnimationActivity
import com.example.navappsample.ui.animation.AnimationScreen
import com.example.navappsample.ui.bottomsheetandsnackbar.BottomSheetActivity
import com.example.navappsample.ui.customview.CustomViewActivity
import com.example.navappsample.ui.loadmorelist.DynamicListActivity
import com.example.navappsample.ui.loadmorelist.DynamicListViewModel
import com.example.navappsample.ui.navigationview.NavigationBarActivity
import com.example.navappsample.ui.navigator.NavigatorActivity
import com.example.navappsample.ui.navigator.NavigatorScreen
import com.example.navappsample.ui.scrollview.ScrollViewActivity
import com.example.navappsample.ui.tabandlist.TabAndListActivity
import com.google.accompanist.flowlayout.FlowColumn
import com.google.accompanist.systemuicontroller.rememberSystemUiController

@OptIn(ExperimentalFoundationApi::class, ExperimentalMaterialApi::class)
@Composable
fun DashBoardScreen(){
    val systemUiController = rememberSystemUiController()
//    systemUiController.isStatusBarVisible = false
    systemUiController.setStatusBarColor(Color.White, true)
    //

    val listFunc = remember {
        mutableStateOf(getListMainFunc())
    }

    val context = LocalContext.current

    Column(
        Modifier
            .fillMaxWidth()
    ) {
        // Creating a Scrollable Box
//        Box(modifier = Modifier
//            .fillMaxSize()
//            .background(Color.White)
//            .verticalScroll(rememberScrollState())
//            .padding(32.dp)) {
////            FlowColumn() {
//
////            }
//            Column(modifier = Modifier.fillMaxSize()) {
//                // Create 6 Scrollable Boxes
////                LazyVerticalGrid(cells = GridCells.Fixed(4),
////
////                    ){
////                    itemsIndexed(listFunc.value){index, item ->
////                        Column(horizontalAlignment = Alignment.CenterHorizontally) {
////                            Text(text = item.title)
////                            Icon(imageVector = item.icon, contentDescription = null)
////                        }
////                    }
////                }
////                Text(text = "this is text")
//            }
//
//        }



        LazyVerticalGrid(columns = GridCells.Fixed(2),
            contentPadding = PaddingValues(10.dp),
            verticalArrangement = Arrangement.spacedBy(10.dp),
            horizontalArrangement = Arrangement.spacedBy(10.dp)
        ){
            item(span = { GridItemSpan(2) }) {
                Box(modifier = Modifier
                    .fillMaxWidth()
                    .requiredHeight(100.dp)
                    .background(Color.Blue)
                ) {
                    Text(
                        text = "this is text",
                        modifier = Modifier.align(Alignment.Center)
                    )
                }

            }
            itemsIndexed(listFunc.value){index, item ->
                Card(
                    onClick ={
                             Log.d("BAO", "-------> click: ${index}")
                        when(index){
                            0 ->{
                                context.startActivity(Intent(context, TabAndListActivity::class.java))
                            }
                            1 ->{
                                context.startActivity(Intent(context, AlertDialogActivity::class.java))
                            }
                            2->{
                                context.startActivity(Intent(context, ScrollViewActivity::class.java))
                            }
                            3 ->{
                                context.startActivity(Intent(context, DynamicListActivity::class.java))
                            }
                            4 ->{
                                val intent = Intent(context, CustomViewActivity::class.java)
                                intent.putExtra("extra", "extra value")
                                context.startActivity(intent)
                            }
                            5 ->{
                                val intent = Intent(context, NavigationBarActivity::class.java)
                                context.startActivity(intent)
                            }
                            6 ->{
                                val intent = Intent(context, BottomSheetActivity::class.java)
                                context.startActivity(intent)
                            }
                            7 ->{
                                val intent = Intent(context, AnimationActivity::class.java)
                                context.startActivity(intent)
                            }
                            8 ->{
                                val intent = Intent(context, NavigatorActivity::class.java)
                                context.startActivity(intent)
                            }
                        }
                    },
                    shape = RoundedCornerShape(10.dp),
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f)
                        .aspectRatio(1f),
                    backgroundColor = Color.White,
                    elevation = 10.dp
                ) {
                    Column(modifier = Modifier
                        .fillMaxSize()
                        .padding(5.dp),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Icon(imageVector = item.icon,
                            contentDescription = null,
                            Modifier
                                .width(50.dp)
                                .height(50.dp)
                        )
                        Text(text = item.title)
                    }
                }
            }
        }
    }
}

private fun getListMainFunc():List<MainFuncModel>{
    return mutableListOf<MainFuncModel>(
        MainFuncModel(0, "Tab Layout and List", Icons.Default.Star),
        MainFuncModel(1, "Alert Dialog", Icons.Default.Star),
        MainFuncModel(2, "Scroll View", Icons.Default.Star),
        MainFuncModel(3, "Dynamic List", Icons.Default.Star),
        MainFuncModel(4, "Custom view", Icons.Default.Star),
        MainFuncModel(5, "Navigation Bar", Icons.Default.Star),
        MainFuncModel(6, "Bottom sheet + Snack bar", Icons.Default.Star),
        MainFuncModel(7, "Animation", Icons.Default.Star),
        MainFuncModel(8, "Navigator", Icons.Default.Star),
        MainFuncModel(2, "Empty", Icons.Default.Star)
    )
}