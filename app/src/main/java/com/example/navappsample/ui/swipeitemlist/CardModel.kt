package com.example.navappsample.ui.swipeitemlist

data class CardModel(val id: Int, val title: String)
