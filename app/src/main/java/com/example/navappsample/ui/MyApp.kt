package com.example.navappsample.ui

import android.app.Application

class MyApp: Application() {

    companion object{
        @Volatile private var INSTANCE: MyApp? = null
        fun  getInstance(): MyApp {
            return INSTANCE?: synchronized(this){
                MyApp().also {
                    INSTANCE = it
                }
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }
}