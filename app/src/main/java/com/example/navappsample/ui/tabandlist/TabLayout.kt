package com.example.navappsample.ui

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.compose.animation.core.tween
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.dp
import com.example.navappsample.R
import com.example.navappsample.common.ViewModelFactory
import com.example.navappsample.ui.tabandlist.TabAndListViewModel
import com.google.accompanist.pager.*
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import kotlinx.coroutines.launch

@OptIn(ExperimentalPagerApi::class)
@Composable
fun TabLayout(){
    val systemUiController = rememberSystemUiController()
//    systemUiController.isStatusBarVisible = false
    systemUiController.setStatusBarColor(Color.White, true)
    val context = LocalContext.current
    val pagerState = rememberPagerState(pageCount = 3, initialOffscreenLimit = 3)
    Column(modifier = Modifier.fillMaxSize()) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .requiredHeight(50.dp)
            .background(Color.Transparent)
            .padding(start = 10.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Box(modifier = Modifier.fillMaxSize()) {
                IconButton(
                    onClick = {
                        Log.d("BAO", "----> click icon back")
                        (context as ComponentActivity).finish()
                    },
                    modifier = Modifier
                        .width(30.dp)
                        .height(30.dp)
                        .align(Alignment.CenterStart)
                ) {
                    Icon(
                        painter = painterResource(R.drawable.ic_back),

                        contentDescription = null,

                        )
                }

                Text(
                    text = "Tab and List",
                    modifier = Modifier
                        .align(Alignment.Center),
                    textAlign = TextAlign.Center
                )
            }
        }
        Divider(color = Color.LightGray, thickness = 1.dp)
        Tabs(pagerState = pagerState)
//         on below line we are calling tabs content
//         for displaying our page for each tab layout
        Log.d("BAO", "-------> onCreate TabLayout")
        TabsContent(pagerState = pagerState)
    }
}


@OptIn(ExperimentalUnitApi::class)
@ExperimentalPagerApi
@Composable
fun Tabs(pagerState: PagerState) {
    // in this function we are creating a list
    // in this list we are specifying data as
    // name of the tab and the icon for it.
    val list = listOf(
        "Home" to Icons.Default.Home,
        "Shopping" to Icons.Default.ShoppingCart,
        "Settings" to Icons.Default.Settings
    )
    // on below line we are creating
    // a variable for the scope.
    val scope = rememberCoroutineScope()
    // on below line we are creating a
    // individual row for our tab layout.
    TabRow(
        // on below line we are specifying
        // the selected index.
        selectedTabIndex = pagerState.currentPage,

        // on below line we are
        // specifying background color.
        backgroundColor = Color.Transparent,

        // on below line we are specifying content color.
        contentColor = Color.LightGray,

        // on below line we are specifying
        // the indicator for the tab
        indicator = { tabPositions ->
            // on below line we are specifying the styling
            // for tab indicator by specifying height
            // and color for the tab indicator.
            TabRowDefaults.Indicator(
                Modifier.pagerTabIndicatorOffset(pagerState, tabPositions),
                height = 2.dp,
                color = Color.Blue,
            )
        }
    ) {
        // on below line we are specifying icon
        // and text for the individual tab item
        list.forEachIndexed { index, _ ->
            // on below line we are creating a tab.
            Tab(
                // on below line we are specifying icon
                // for each tab item and we are calling
                // image from the list which we have created.

                icon = {
                    Icon(imageVector = list[index].second,
                        contentDescription = null,
                        tint = if (pagerState.currentPage == index) Color.Blue else Color.LightGray
                    )
                },
                // on below line we are specifying the text for
                // the each tab item and we are calling data
                // from the list which we have created.
                text = {
                    Text(
                        list[index].first,
                        // on below line we are specifying the text color
                        // for the text in that tab
                        color = if (pagerState.currentPage == index) Color.Blue else Color.LightGray
                    )
                },
                // on below line we are specifying
                // the tab which is selected.
                selected = pagerState.currentPage == index,
                // on below line we are specifying the
                // on click for the tab which is selected.
                onClick = {
                    // on below line we are specifying the scope.
                    scope.launch {
//                        pagerState.scrollToPage(index)
                        pagerState.animateScrollToPage(index)
                    }
                }
            )
        }
    }
}

@ExperimentalPagerApi
@Composable
fun TabsContent(pagerState: PagerState) {
    // on below line we are creating
    // horizontal pager for our tab layout.
    HorizontalPager(state = pagerState) {
        // on below line we are specifying
        // the different pages.
            page ->
        when (page) {
            // on below line we are calling tab content screen
            // and specifying data as Home Screen.
            0 -> TabContentScreen(data = "Welcome to Home Screen")
            // on below line we are calling tab content screen
            // and specifying data as Shopping Screen.
            1 -> TabContentScreen(data = "Welcome to Shopping Screen")
            // on below line we are calling tab content screen
            // and specifying data as Settings Screen.
            2 -> TabContentScreen(data = "Welcome to Settings Screen")
        }
    }
}


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun TabContentScreen(data: String) {
    // on below line we are creating a column
    Log.d("BAO", "-----> create view content: $data")
    val homeViewModel = remember {
        ViewModelFactory().create(TabAndListViewModel::class.java)
    }
    val context = LocalContext.current
    Column(
        // in this column we are specifying modifier
        // and aligning it center of the screen on below lines.
        modifier = Modifier.fillMaxSize()
    ) {
        // in this column we are specifying the text
        Log.d("BAO", "-----> create view content1: $data")
        Text(
            // on below line we are specifying the text message
            text = data,

            // on below line we are specifying the text style.
            style = MaterialTheme.typography.h5,

            // on below line we are specifying the text color
            color = Color.Green,

            // on below line we are specifying the font weight
            fontWeight = FontWeight.Bold,

            //on below line we are specifying the text alignment.
            textAlign = TextAlign.Center
        )
        CreateTextView(viewModel = homeViewModel)
        LazyColumn(modifier = Modifier
            .fillMaxSize()
            .padding(start = 10.dp, end = 10.dp)){
            itemsIndexed(homeViewModel.listData
            ){ index, item ->
                Button(
                    onClick = {
                        Log.d("BAO", "------> click before: ${homeViewModel.test}")
                        homeViewModel.test = index.toString()
                        homeViewModel.index.value = index
                        homeViewModel.updateOneItem(index)
                        Log.d("BAO", "------> click affter: ${homeViewModel.test}")
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .defaultMinSize(minHeight = 50.dp)
                        .animateItemPlacement(animationSpec = tween(1000))
                ) {
                    Text(text = item)
                }
                
                Spacer(modifier = Modifier.height(10.dp))
            }
        }
    }
}

fun <T>intentToEmpty(context:Context, target:Class<T>){
    context.startActivity(Intent(context, target))
}

@Composable
fun CreateTextView(viewModel: TabAndListViewModel){
    if (viewModel.index.value > 4){
        Log.d("BAO", "------> index: ${viewModel.index.value}")
        Text(
            // on below line we are specifying the text message
            text = "Index is > 4",

            // on below line we are specifying the text style.
            style = MaterialTheme.typography.h5,

            // on below line we are specifying the text color
            color = Color.Green,

            // on below line we are specifying the font weight
            fontWeight = FontWeight.Bold,

            //on below line we are specifying the text alignment.
            textAlign = TextAlign.Center
        )
    }
}