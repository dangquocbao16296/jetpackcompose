package com.example.navappsample.ui.bottomsheetandsnackbar

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.navappsample.common.base.MyHeader
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun BottomSheetScreen(){
    val bottomSheetScaffoldState = rememberBottomSheetScaffoldState(
        bottomSheetState = rememberBottomSheetState(
            initialValue = BottomSheetValue.Collapsed
        )
    )


    val sheetState = rememberModalBottomSheetState(initialValue = ModalBottomSheetValue.Hidden)
    
    BottomSheetView(sheetState = sheetState)

}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun BottomSheetView(sheetState:ModalBottomSheetState){
    val corountineScope = rememberCoroutineScope()
//    val corountineScope1 = rememberCoroutineScope()
//    val sheetState1 = rememberModalBottomSheetState(initialValue = ModalBottomSheetValue.Hidden)
//    InnerBottomSheet(sheetState = sheetState1)
    var style = remember {
        false
    }
    ModalBottomSheetLayout(sheetContent = {
        if (style){
            Column(modifier = Modifier
                .fillMaxWidth()
                .height(200.dp)
                .background(Color.Blue)
            ) {

                Text(text = "Bottom sheet 1", textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth()
                )
                Button(onClick = {}) {
                    Text(text = "Click me")
                }
            }
        }else{
            Column(modifier = Modifier
                .fillMaxWidth()
                .height(200.dp)
            ) {

                Text(text = "Bottom sheet", textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth()
                )
                Button(onClick = {}) {
                    Text(text = "Click me")
                }
            }
        }
    },
        sheetElevation = 5.dp,
        sheetState = sheetState,
        sheetShape = RoundedCornerShape(topStartPercent = 15, topEndPercent = 15)
    ){
        val scaffState = rememberScaffoldState()
        Scaffold(
            modifier = Modifier.fillMaxSize(),
            topBar = {
                MyHeader("Bottom sheet + Snack bar")
            },
            scaffoldState = scaffState
        ){
            Column(modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp)) {
                Button(onClick = { corountineScope.launch { sheetState.show() } }
                ) {
                    Text(text = "Click me to show bottom sheet")
                }
                Button(onClick = {
                    corountineScope.launch {
                        val result = scaffState.snackbarHostState.showSnackbar(
                            message = "this is mess",
                            actionLabel = "this is action label",
                            duration = SnackbarDuration.Long
                        )
                    }
                }) {
                    Text(text = "Click me to show snack bar")
                }
                Button(onClick = { /*TODO*/ }) {
                    Text(text = "Click me")
                }
            }
        }
    }
}


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun InnerBottomSheet(sheetState:ModalBottomSheetState){
    ModalBottomSheetLayout(sheetContent = {
        Column(modifier = Modifier
            .fillMaxWidth()
            .height(200.dp)
        ) {
            Text(text = "Inner Bottom sheet", textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth()
            )
        }
    },
        sheetElevation = 5.dp,
        sheetState = sheetState,
        sheetShape = RoundedCornerShape(topStartPercent = 15, topEndPercent = 15)
    ){
        Column() {
            
        }
    }
}