package com.example.navappsample.ui.loadmorelist

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.navappsample.common.Result
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DynamicListViewModel:ViewModel() {
    val repo = Respository()
    var startIndex = 0
    var _listData = mutableStateListOf<ItemList>()
    val listData:List<ItemList> = _listData
    var isLoading = false
    var isEnd:Boolean = false
    var _isLoading1 = mutableStateOf(false)
    var isLoading1:State<Boolean> = _isLoading1
    var isDeleteMode = mutableStateOf(false)
    init {
        getItems(0)
    }
    fun getItems(startIndex:Int){
        viewModelScope.launch(Dispatchers.IO) {

            withContext(Dispatchers.Main){
                _isLoading1.value = true
            }
            isLoading = true

            val result = withContext(Dispatchers.IO){
                repo.getItems(startIndex)
            }
            isLoading = false
            withContext(Dispatchers.Main){
                _isLoading1.value = false
            }
            isLoading = false
            when(result){
                is Result.Success ->{
                    val data = result.data
                    if (data?.isEmpty() != true){
                        Log.d("BAO", "-----> list item: ${Gson()}")
                        data?.let { _listData.addAll(it.toList()) }
                    }else{
                        withContext(Dispatchers.Main){
//                            isLoading = false
                        }
                        isEnd = true
                    }
                }
                else ->{

                }
            }
        }
    }

    fun updateTest(index:Int){
        _listData[index].test.value = "updated test"
    }

    fun deleteItem(index:Int){
        _listData.removeAt(index)
    }
    fun updateItem(index:Int){
        _listData[index] = _listData[index].copy(
            description = "clicked"
        )
    }
    fun longClickItem(index: Int){
        if (!isDeleteMode.value) isDeleteMode.value = true
        if (isDeleteMode.value){
            if (!_listData[index].isSelected){
                _listData[index].isSelected = true
                _listData[index].color.value = Color.Blue
            }
        }else{
//            isDeleteMode.value = true
//            _listData[index] = _listData[index].copy(isSelected = true)
        }
//        if (!_listData[index].isSelected){
//            _listData[index] = _listData[index].copy(isSelected = )
//        }

    }
    fun clickItem(index: Int){
        if (isDeleteMode.value){
            if (listData[index].isSelected){
                _listData[index].isSelected = false
                _listData[index].color.value = Color.DarkGray
            }else{
                _listData[index].isSelected = true
                _listData[index].color.value = Color.Blue
            }
        }else{

        }
    }

    fun onHideModeDelete(){
        _listData.map {
            if (it.isSelected){
                it.isSelected = false
                it.color.value = Color.DarkGray
            }
        }
//        _listData[0] = _listData[0].copy(isSelected = false)

//        _listData.set = result

//        _listData.forEachIndexed { index, itemList ->
//            if (itemList.isSelected){
//                itemList.isSelected = false
//                _listData.set(index, itemList.copy(isSelected = false))
//            }
//        }

//        _listData.replaceAll {
//            it.isSelected = false
//            it
//        }

    }
}

class State(){

}