package com.example.navappsample.ui.alertdiog

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.navappsample.R
import com.example.navappsample.common.Constant
import com.example.navappsample.ui.MyApp

class NotificationBarHepler {



    private val appContext by lazy {
        MyApp.getInstance().applicationContext
    }

    private val notifiManager: NotificationManager by lazy {
        appContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    companion object{
        @Volatile private var INSTANCE: NotificationBarHepler? = null
        fun  getInstance(): NotificationBarHepler {
            return INSTANCE?: synchronized(this){
                NotificationBarHepler().also {
                    INSTANCE = it
                }
            }
        }
    }

    fun showNormalNotification(){
        initNotifiChanel()
        var builder = NotificationCompat.Builder(appContext, Constant.CHANNEL_NOTIFICATION)
            .setSmallIcon(R.drawable.ic_notification)
            .setContentTitle("this is title")
            .setContentText("this is content")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        val notificationCompat = NotificationManagerCompat.from(appContext)
        notificationCompat.notify(Constant.NOTIFICATION_ID, builder.build())
    }

    fun clearAllNotifi(){
        notifiManager.cancelAll()
    }

    fun showCustomNotifi(){
        initNotifiChanel()

        var remoteViews: RemoteViews? = null
        remoteViews = RemoteViews(appContext?.packageName, R.layout.custom_notification_bar)
        remoteViews?.setTextViewText(R.id.tv_test, "Custom notification bar")

        var builder = NotificationCompat.Builder(appContext, Constant.CHANNEL_NOTIFICATION)
            .setContent(remoteViews)
            .setSmallIcon(R.drawable.ic_notification)
            .setCategory(NotificationCompat.CATEGORY_SERVICE)
            .setOnlyAlertOnce(true)
            //.setOngoing(true)
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//            .priority = NotificationManagerCompat.IMPORTANCE_HIGH
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
        }

        val notificationCompat = NotificationManagerCompat.from(appContext)
        notificationCompat.notify(Constant.NOTIFICATION_ID, builder.build())
    }

    fun initNotifiChanel(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O && notifiManager.getNotificationChannel(Constant.CHANNEL_NOTIFICATION) == null
        ) {
            val name = Constant.CHANNEL_NAME_NOTIFICATION
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(Constant.CHANNEL_NOTIFICATION, name, importance)
            channel.enableVibration(false)
            notifiManager.createNotificationChannel(channel)
        }
    }
}