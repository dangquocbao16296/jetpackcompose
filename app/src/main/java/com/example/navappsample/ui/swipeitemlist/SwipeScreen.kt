import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Call
import androidx.compose.material.icons.outlined.Delete
import androidx.compose.material.icons.outlined.Star
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.Role.Companion.Button
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.navappsample.R
import de.charlex.compose.RevealDirection
import de.charlex.compose.RevealSwipe
import me.saket.swipe.SwipeAction
import me.saket.swipe.SwipeableActionsBox

@Composable
fun SwipeScreen(){
    Column(Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {

        // Fetching local context
        val mLocalContext = LocalContext.current

        // Creating a Swipe Action for Calling;
        // setting icon, background and what
        // happens when swiped
        val mCall = SwipeAction(
            icon = painterResource(R.drawable.ic_launcher_background),
            background = Color.Green,
            isUndo = true,
            onSwipe = { Toast.makeText(mLocalContext, "Calling",Toast.LENGTH_SHORT).show()}
        )

        // Creating a Swipe Action for Sending a message;
        // setting icon, background and what happens when swiped
        val mMessage = SwipeAction(
            icon = painterResource(R.drawable.ic_launcher_background),
            background = Color.Yellow,
            isUndo = true,
            onSwipe = { Toast.makeText(mLocalContext, "Sending Message",Toast.LENGTH_SHORT).show()}
        )

        // Creating a Swipe Action Box with start
        // action as calling and end action as sending message
        SwipeableActionsBox(startActions = listOf(mCall), endActions = listOf(mMessage)) {

            // Creating a Button inside Swipe Action Box
            Button(onClick = { /*TODO*/ }, colors = ButtonDefaults.buttonColors(backgroundColor = Color(0XFF0F9D58))) {
                Text(text = "Swipe Left or Right", fontSize = 25.sp, color = Color.White)
            }
        }
    }
}


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SwipeScreen1(){
    Column(modifier = Modifier.height(200.dp)) {
        RevealSwipe(
            modifier = Modifier.padding(vertical = 5.dp),
            hiddenContentStart = {
                Icon(
                    modifier = Modifier.padding(horizontal = 25.dp),
                    imageVector = Icons.Outlined.Star,
                    contentDescription = null,
                    tint = Color.DarkGray
                )
            },
            hiddenContentEnd = {
                Icon(
                    modifier = Modifier.padding(horizontal = 25.dp),
                    imageVector = Icons.Outlined.Delete,
                    contentDescription = null,
                    tint = Color.DarkGray
                )
            }
        ) {
            Card(
                modifier = Modifier
                    .fillMaxSize()
                    .requiredHeight(80.dp),
                backgroundColor = Color.Blue,
                shape = it,
            ){
                Text(
                    modifier = Modifier.padding(start = 20.dp, top = 20.dp),
                    text = "this is text"
                )
            }
        }
    }

}

@Composable
fun SwipeScreen2(){

}
