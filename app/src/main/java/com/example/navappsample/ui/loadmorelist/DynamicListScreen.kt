package com.example.navappsample.ui.loadmorelist

import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.semantics.Role.Companion.Image
import androidx.compose.ui.unit.dp
import coil.compose.SubcomposeAsyncImage
import coil.compose.rememberAsyncImagePainter
import com.example.navappsample.common.base.baseHeader
import com.example.navappsample.ui.scrollview.ScrollViewActivity

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun DynamicListActivity.DynamicListScreen(){
    Column(modifier = Modifier
        .fillMaxSize()
    ) {
        baseHeader(title = "Load more list",
            viewModel.isDeleteMode,
            onBackPressDeleteMode = {
                viewModel.onHideModeDelete()
            }
        )
        LazyColumn(modifier = Modifier
            .fillMaxWidth()
            .padding(0.dp),
            contentPadding = PaddingValues(10.dp)
        ){
            items(viewModel.listData.size){ index ->
                if (index >= viewModel.listData.size -1 && !viewModel.isLoading && !viewModel.isEnd){
                    viewModel.getItems(index)
                }

                var colorBorder = if (viewModel.listData[index].isSelected) Color.Blue else Color.DarkGray
                Column(modifier = Modifier
                    .padding(start = 0.dp, end = 0.dp, top = 15.dp)
                    .fillMaxWidth(1f)
                    .border(BorderStroke(1.dp, viewModel.listData[index].color.value),
                        shape = RoundedCornerShape(5.dp)
                    )
                    .combinedClickable(
                        onLongClick = {
                            Log.d("BAO", "------> onLong click: ${index}")
                            viewModel.longClickItem(index)
                        },
                        onClick = { viewModel.clickItem(index) }
                    )
                ) {
                    Log.d("BAO", "Position: $index")

//                    Image(
//                        painter = rememberAsyncImagePainter("https://reqres.in/img/faces/1-image.jpg"),
//                        contentDescription = null,
//                        modifier = Modifier.size(70.dp)
//                    )

                    SubcomposeAsyncImage(
                        model = "https://reqres.in/img/faces/1-image.jpg",
                        loading = {
                            CircularProgressIndicator()
                        },
                        contentDescription = "",
                        modifier = Modifier.size(70.dp)
                    )


                    Text(text = viewModel.listData[index].title,
                        Modifier.padding(0.dp)
                    )
                    Text(text = viewModel.listData[index].description,
                        Modifier.padding(0.dp)
                    )
                    Text(text = viewModel.listData[index].test.value)

                    Button(onClick = {
                        viewModel.updateTest(index)
                    }
                    ) {
                        Text(text = "Update test")
                    }

                    Button(onClick = {
                        viewModel.deleteItem(index)
                    }
                    ) {
                        Text(text = "Delete")
                    }
                    Button(onClick = {
                        viewModel.updateItem(index)
                    }
                    ) {
                        Text(text = "Update")
                    }
                    if (viewModel.listData[index].isSelected){
                        Icon(imageVector = Icons.Default.Check,
                            modifier = Modifier.size(30.dp),
                            contentDescription = "Selected"
                        )
                    }
                }

            }

//            item {
//                showLoad(viewModel = viewModel)
//            }

            item{
                if (viewModel.isLoading1.value){
                    Row(modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        CircularProgressIndicator(color = Color.DarkGray)
                    }
                }
            }

        }
    }
}
@Composable
fun showLoad(viewModel:DynamicListViewModel){
    if (viewModel.isLoading){
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            CircularProgressIndicator(color = Color.DarkGray)
        }
    }
}

@Composable
fun showLoad1(){

}