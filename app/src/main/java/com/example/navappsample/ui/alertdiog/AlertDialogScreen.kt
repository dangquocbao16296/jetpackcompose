package com.example.navappsample.ui.alertdiog

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.example.navappsample.R
import com.example.navappsample.common.base.MyButton
import com.example.navappsample.common.base.baseHeader


@Composable
fun AlertDialogActivity.AlertDialogScreen(){
    Column(
        modifier = Modifier.fillMaxSize(),
    ) {
        baseHeader("Dialog View")
        Button(
            onClick = { showCustomDialogWithResult?.value = true },
        ) {
            Text(text = "Show alert dialog")
        }
        
        MyButton() {
            Text(
                text = "this my button text",
                modifier = Modifier
                    .background(color = Color.Transparent)
                    .paddingFromBaseline(top = 0.dp),
                textAlign = TextAlign.Center
            )
        }

        val context = LocalContext.current

        Button(
            onClick = {


//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    val name: CharSequence = "name channel"
//                    val description = "desciption"
//                    val importance = NotificationManager.IMPORTANCE_DEFAULT
//                    val channel = NotificationChannel("CHANNEL_ID", name, importance)
//                    channel.description = description
//                    // Register the channel with the system; you can't change the importance
//                    // or other notification behaviors after this
//                    val notificationManager: NotificationManager? = ContextCompat.getSystemService(context,
//                        NotificationManager::class.java)
//                    notificationManager!!.createNotificationChannel(channel)
//                }
//
//                var builder = NotificationCompat.Builder(context, "CHANNEL_ID")
//                    .setSmallIcon(R.drawable.ic_notification)
//                    .setContentTitle("this is title")
//                    .setContentText("this is content")
//                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//
//
//
//                with(NotificationManagerCompat.from(context)) {
//                    // notificationId is a unique int for each notification that you must define
//                    notify(123, builder.build())
//                }
                NotificationBarHepler.getInstance().showNormalNotification()
            }
        ) {
            Text(text = "Show notification")
        }

        Button(onClick = {
            NotificationBarHepler.getInstance().showCustomNotifi()
        }) {
            Text(text = "Show custom notifi")
        }

        Button(onClick = {
            NotificationBarHepler.getInstance().clearAllNotifi()
        }
        ) {
            Text(text = "Hide notification")
        }
    }
}

