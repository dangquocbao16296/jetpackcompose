package com.example.navappsample.ui.tabandlist

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel

class TabAndListViewModel: ViewModel() {
    var test = "test"
    val index = mutableStateOf(0)

    val _listData = mutableStateListOf<String>()
    val listData:List<String> = _listData

    init {
        getList()
    }
    fun getList(){
        val listData = ArrayList<String>()
        for (i in 0..20){
            listData.add("Index: ${i}")
        }
        _listData.addAll(listData)
    }

    fun updateOneItem(index:Int){
//        val data = _listData.value[index]
        _listData[index] = "clicked"
//        _itemList[index] = _itemList[index].copy(weight = newVal)
    }
}