package com.example.navappsample.ui.animation

import android.annotation.SuppressLint
import android.view.animation.BounceInterpolator
import androidx.compose.animation.*
import androidx.compose.animation.core.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.navappsample.common.base.MyButton
import com.example.navappsample.common.base.MyHeader

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun AnimationScreen(){
    Scaffold(modifier = Modifier.fillMaxSize(),
        topBar = {
            MyHeader("Animation")
        }
    ) {
        Column(
            Modifier
                .fillMaxSize()
                .padding(5.dp)
        ) {
//            val showText by remember {
//                mutableStateOf(true)
//            }
//            AnimatedVisibility(visible = showText) {
//                MyButton(modifier = Modifier.fillMaxWidth()) {
//                    Text(text = "Click me")
//                }
//                AnimateVisibility()
//            }
//            AnimatedVisibility(visible = showText) {
//                MyButton(modifier = Modifier.fillMaxWidth()) {
//                    Text(text = "Click me")
//                }
//                AnimateVisibility1()
//            }
            AnimateVisibility1()
            AnimateVisibility1()
        }
    }
}

@Composable
fun AnimateVisibility() {
    var visible by remember {
        mutableStateOf(true)
    }
    Column (modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally) {
        AnimatedVisibility(visible = visible,
            enter = fadeIn(animationSpec = tween(1000)),
            exit = fadeOut(animationSpec = tween(1000))
        ) {
            Text(text = "Hello, world!")
        }
        Button(onClick = { visible = !visible }) {
            Text("Click Me")
        }
    }
}

@Composable
fun AnimateVisibility1() {
    var visible by remember {
        mutableStateOf(true)
    }
    Column (modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally) {
        AnimatedVisibility(visible = visible,
            enter = fadeIn(animationSpec = tween(1000)) +
                    expandVertically (
                animationSpec = tween(1500,
                    easing = LinearEasing)),
            exit = fadeOut(animationSpec = tween(1000)) +
                    shrinkVertically (
                        animationSpec = tween(1500,
                            easing = LinearOutSlowInEasing))
        ) {
            Text(text = "Hello, world!")
        }
        Button(onClick = { visible = !visible }) {
            Text("Click Me")
        }
    }
}