package com.example.navappsample.ui.navigator

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.navappsample.common.base.MyHeader

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun NavigatorScreen(){
    val navController = rememberNavController()
    Scaffold(modifier = Modifier.fillMaxSize(),
        topBar ={
            MyHeader("Navigator")
        }
    ) {
        NavHost(navController = navController, "screen1"){
            composable("screen1",
//                arguments = listOf(
//                    navArgument("name"){
//                        type = NavType.StringType
//                        defaultValue = "Hello"
//                        nullable = true
//                    }
//                )
            ){
//                val customerName = it.arguments?.getString("name")
                Screen1("screen1",navController = navController)
            }
            composable("screen2" + "/{name}",
                arguments = listOf(
                    navArgument("name"){
                        type = NavType.StringType
                        defaultValue = "aaaa"
                        nullable = true
                    }
                )
            ){
                val name = it.arguments?.getString("name")
                Screen2(name ?: "",navController = navController)
            }
        }
//        Column(modifier = Modifier
//            .fillMaxSize()
//            .padding(5.dp)
//        ) {
//            Button(onClick = { navController.navigate("1/android") }) {
//                Text(text = "Click")
//            }
//        }
    }
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun Screen1(name:String = "", navController: NavController){
    Scaffold(modifier = Modifier.fillMaxSize()) {
        Column(modifier = Modifier
            .fillMaxSize()
            .padding(5.dp)) {
            Text(text = name, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth())
            Button(onClick = { navController.navigate("screen2/screen222222") {
                popUpTo(navController.graph.startDestinationId){
                    saveState = true
                }
                launchSingleTop = true
                restoreState = true
            }
            }
            ) {
                Text(text = "screen2")
            }
        }
    }
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun Screen2(name:String = "", navController: NavController){
    Scaffold(modifier = Modifier.fillMaxSize()) {
        Column(modifier = Modifier
            .fillMaxSize()
            .padding(5.dp)) {
            Text(text = name, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth())
        }
    }
}

