package com.example.navappsample.ui.navigationview

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel

class EmptyViewModel1:ViewModel(){
    val text = mutableStateOf<String>("")
//    val text:String = _text.value


    fun updateText(){
        text.value = "updated"
    }
}