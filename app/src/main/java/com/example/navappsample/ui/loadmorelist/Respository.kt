package com.example.navappsample.ui.loadmorelist

import androidx.compose.material.ListItem
import com.example.navappsample.common.Result
import kotlinx.coroutines.delay

class Respository {
    private val remoteDataSource = (0..50).map {
        ItemList(title = "Title: $it", description = "Description: $it", isLoading = false)
    }

    suspend fun getItems(startIndex:Int):Result<List<ItemList>>{
        delay(2000L)
        return if (startIndex + 20 <= remoteDataSource.size){
            Result.Success(remoteDataSource.subList(startIndex, startIndex + 20))
        }else Result.Success(emptyList())
    }
}