package com.example.navappsample.ui.dashboard

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.ui.graphics.vector.ImageVector

data class MainFuncModel(
    val id:Int,
    val title:String,
    val icon: ImageVector
)
