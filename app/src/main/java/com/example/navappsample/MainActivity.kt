package com.example.navappsample

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import com.example.navappsample.common.Routes
import com.example.navappsample.common.ViewModelFactory
import com.example.navappsample.ui.tabandlist.TabAndListViewModel
import com.example.navappsample.ui.TabLayout
import com.example.navappsample.ui.dashboard.DashBoardScreen
import com.example.navappsample.ui.theme.NavAppSampleTheme
import com.google.accompanist.pager.ExperimentalPagerApi

class MainActivity : ComponentActivity() {
    val viewModel by viewModels<TabAndListViewModel> { ViewModelFactory() }
    override fun onCreate(savedInstanceState: Bundle?) {
        @OptIn(ExperimentalPagerApi::class)
        super.onCreate(savedInstanceState)
        setContent {
            Log.d("BAO", "------> onCreate main view")
            NavAppSampleTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
//                    val navController = rememberNavController()
//                    NavHost(
//                        navController = navController,
//                        startDestination = Routes.Home.route
//                    ){
//                        composable(Routes.Home.route){
//                            Greeting("Android", navController)
//                        }
//                        composable(Routes.Home1.route){
//                            Greeting1("Android1",navController)
//                        }
//                        composable(Routes.Home2.route){
//                            Greeting2("Android2")
//                        }
//                        composable(Routes.Home3.route){
//                            Greeting3("Android3")
//                        }
//                        composable(Routes.Tablayout.route){
//                            TabLayout()
//                        }
//                    }
//                    TabLayout()
                    DashBoardScreen()
                }
            }
        }
    }
}



@Composable
fun Greeting(name: String, navController: NavController? = null) {
    Text(text = "Hello $name!")
    Button(onClick = {
        navController?.navigate(route = Routes.Tablayout.route)
    }) {
        Text(text = "Move to tablayout")
    }
}

@Composable
fun Greeting1(name: String, navController: NavController? = null) {
    Text(text = "Hello $name!")
    Button(onClick = {
        navController?.navigate(route = Routes.Home2.route){
            popUpTo(Routes.Home.route)
        }
    }) {
        Text(text = "Move to home2")
    }
}

@Composable
fun Greeting2(name: String) {
    Text(text = "Hello $name!")
}

@Composable
fun Greeting3(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    NavAppSampleTheme {
        TabLayout()
    }
}