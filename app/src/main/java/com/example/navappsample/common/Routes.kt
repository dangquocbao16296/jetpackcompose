package com.example.navappsample.common

sealed class Routes(val route:String){
    object Home:Routes(route = "Home")
    object Home1:Routes(route = "Home1")
    object Home2:Routes(route = "Home2")
    object Home3:Routes(route = "Home3")
    object Tablayout:Routes(route = "Tablayout")
}


sealed class RoutesNav(val route:String){
    object Home:RoutesNav(route = "Home")
    object Contact:RoutesNav(route = "Contact")
    object Setting:RoutesNav(route = "Setting")
}