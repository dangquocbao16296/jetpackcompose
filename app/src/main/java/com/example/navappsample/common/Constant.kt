package com.example.navappsample.common

class Constant {
    companion object{
        val CHANNEL_NAME_NOTIFICATION = "CHANNEL_NOTIFICATION"
        val CHANNEL_NOTIFICATION = "CHANNEL_NOTIFICATION"
        val NOTIFICATION_ID = 111
    }
}