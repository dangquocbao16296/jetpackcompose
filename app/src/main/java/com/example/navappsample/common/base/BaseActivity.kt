package com.example.navappsample.common.base

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.example.navappsample.R
import com.example.navappsample.ui.alertdiog.AlertDialogScreen
import com.example.navappsample.ui.ui.theme.NavAppSampleTheme
import com.example.navappsample.ui.ui.theme.Purple200
import com.google.accompanist.systemuicontroller.rememberSystemUiController

open class BaseActivity: ComponentActivity() {
    var showCustomDialogWithResult:MutableState<Boolean>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
//            val systemUiController = rememberSystemUiController()
//            systemUiController.setStatusBarColor(Color.Red, true)
        }
    }
}

@Composable
fun BaseActivity.setupAlerDialog(){
    showCustomDialogWithResult = remember { mutableStateOf(false) }
    showDialog(showCustomDialogWithResult = showCustomDialogWithResult ?: remember { mutableStateOf(false)})
}

@Composable
fun showDialog(showCustomDialogWithResult: MutableState<Boolean>){
    val context = LocalContext.current
    var showCustomDialogWithResult = showCustomDialogWithResult


    if (showCustomDialogWithResult.value) {
        CustomDialogWithResultExample(
            onDismiss = {
                showCustomDialogWithResult.value = !showCustomDialogWithResult.value
                Toast.makeText(context, "Dialog dismissed!", Toast.LENGTH_SHORT)
                    .show()
            },
            onNegativeClick = {
                showCustomDialogWithResult.value = !showCustomDialogWithResult.value
                Toast.makeText(context, "Negative Button Clicked!", Toast.LENGTH_SHORT)
                    .show()

            },
            onPositiveClick = {
                showCustomDialogWithResult.value = !showCustomDialogWithResult.value
                Toast.makeText(context, "Selected color: ", Toast.LENGTH_SHORT)
                    .show()
            }
        )
    }

}

@Composable
private fun CustomDialogWithResultExample(
    onDismiss: () -> Unit,
    onNegativeClick: () -> Unit,
    onPositiveClick: () -> Unit
) {
//    var red = remember { mutableStateOf(0f) }
//    var green = remember { mutableStateOf(0f) }
//    var blue = remember { mutableStateOf(0f) }
//
//    val color = Color(
//        red = red,
//        green = green,
//        blue = blue,
//        alpha = 255
//    )

    Dialog(onDismissRequest = onDismiss,
        properties = DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false)
    ) {

        Card(
            elevation = 0.dp,
            shape = RoundedCornerShape(5.dp)
        ) {

            Column(modifier = Modifier.padding(0.dp)) {

                Column(modifier = Modifier.padding(20.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "This is title",
                        fontWeight = FontWeight.Bold,
                        fontSize = 20.sp,
                        textAlign = TextAlign.Center
                    )
                    Spacer(modifier = Modifier.height(8.dp))

                    // content
                    Text(text = "this is title this is title this is title this is title this is title",
                        textAlign = TextAlign.Center
                    )
                }
                // title


                Spacer(modifier = Modifier.height(8.dp))
                // Buttons
                Row(
                    horizontalArrangement = Arrangement.SpaceAround,
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Purple200)
                ) {

                    TextButton(onClick = onNegativeClick,
                        modifier = Modifier.padding(0.dp)
                    ) {
                        Text(text = "CANCEL", textAlign = TextAlign.Center)
                    }

                    TextButton(onClick = {
                        onPositiveClick() },
                        modifier = Modifier
                            .padding(0.dp)
                    ) {
                        Text(text = "OK", textAlign = TextAlign.Center)
                    }
                }
            }
        }
    }
}

@Composable
fun BaseActivity.baseHeader(title:String?="",
                            isDeleteMode:MutableState<Boolean> = mutableStateOf(false),
                            onBackPressDeleteMode:(() -> Unit)? = null
){
    val context = LocalContext.current
    Column(modifier = Modifier.fillMaxWidth()) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .requiredHeight(50.dp)
            .background(Color.Transparent)
            .padding(start = 10.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Box(modifier = Modifier.fillMaxSize()) {
                IconButton(
                    onClick = {
                        Log.d("BAO", "----> click icon back")
                        if (isDeleteMode.value){
                            isDeleteMode.value = false
                            onBackPressDeleteMode?.invoke()
                        }else{
                            (context as ComponentActivity).finish()
                        }
                    },
                    modifier = Modifier
                        .width(30.dp)
                        .height(30.dp)
                        .align(Alignment.CenterStart)
                ) {
                    Icon(
                        painter = painterResource(R.drawable.ic_back),

                        contentDescription = null,

                        )
                }

                Text(
                    text = title ?: "",
                    modifier = Modifier
                        .align(Alignment.Center),
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Bold
                )

                if (isDeleteMode.value){
                    IconButton(
                        onClick = {
                            Log.d("BAO", "----> click icon delete")

                        },
                        modifier = Modifier
                            .width(30.dp)
                            .height(30.dp)
                            .align(Alignment.CenterEnd)
                    ) {
                        Icon(
                            imageVector = Icons.Default.Delete,

                            contentDescription = null,

                            )
                    }
                }
            }
        }

        Divider(color = Color.LightGray, thickness = 1.dp)
    }
}