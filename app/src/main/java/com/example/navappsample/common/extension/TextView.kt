package com.example.navappsample.common.extension

import android.widget.TextView
import androidx.compose.ui.unit.dp

fun TextView.type12(){
    this.textSize = 12.dp.value
}