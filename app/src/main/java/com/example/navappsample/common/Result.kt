package com.example.navappsample.common

sealed class Result<T>(val data:T? = null, val isLoading:Boolean? = null){
    class Success<T>(data: T): Result<T>(data)
    class Loading<T>(isLoading: Boolean):Result<T>(isLoading = isLoading)
}
