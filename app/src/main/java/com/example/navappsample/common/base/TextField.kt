package com.example.navappsample.common.base

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.InteractionSource
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.outlined.Close
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.focus.FocusState
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.navappsample.ui.customview.CustomViewScreen
import com.example.navappsample.ui.theme.NavAppSampleTheme

@Composable
fun MyOutlinedTextField(value:String = "",
                        label:String = "",
                        modifier: Modifier = Modifier,
                        isError:Boolean = false,
                        errorMessage:String? = null,
                        onClickErrorIcon:(() -> Unit)? = null,
                        onValueChanged:((String?) -> Unit)? = null,
                        onFocusedChange:((FocusState?) -> Unit)? = null
){

    Column(modifier = modifier.fillMaxWidth()) {
        OutlinedTextField(
            value = value,
            onValueChange ={
                onValueChanged?.let { it1 -> it1(it) }
            },
            modifier = Modifier
                .defaultMinSize(minHeight = 20.dp)
                .fillMaxWidth(),
            textStyle = TextStyle(fontSize = 12.sp),
            label = { Text(text = label, fontSize = 12.sp) },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Color.Blue,
                focusedLabelColor = Color.Blue,
                unfocusedBorderColor = Color.DarkGray,
                unfocusedLabelColor = Color.DarkGray,
                cursorColor = Color.Black
            ),
            isError = isError,
            trailingIcon = {
                if (isError)
                    Icon(
                        Icons.Filled.Close,
                        "Error",
                        tint = MaterialTheme.colors.error,
                        modifier = Modifier.clickable {
                            Log.d("BAO", "---------> click err icon")
                            onClickErrorIcon?.invoke()
                        }
                    )
            }
        )
        if (!errorMessage.isNullOrEmpty()){
            Text(
                text = errorMessage ?: "",
                color = MaterialTheme.colors.error,
                style = MaterialTheme.typography.caption,
                modifier = Modifier
                    .padding(top = 0.dp, end = 0.dp, bottom = 0.dp)
            )
        }
    }
}

@Composable
fun LabelAndPlaceHolder(modifier: Modifier = Modifier
    .fillMaxWidth()
    .padding(start = 5.dp, end = 5.dp)
) {
    var text = remember { mutableStateOf("") }
    TextField(
        modifier = modifier
            .padding(0.dp),
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Color.Transparent,
            focusedLabelColor = Color.Blue,
            unfocusedLabelColor = Color.DarkGray
        ),
        value = text.value,
        onValueChange = {
            text.value = it
        },
        label = { Text(text = "Your Label",
            Modifier.padding(0.dp)
        ) }
//        placeholder = { Text(text = "Your Placeholder/Hint") },

    )
}


@Composable
fun BackGroundTextField(){
    Column {
        var textState = remember { mutableStateOf("") }
        val maxLength = 110
        val lightBlue = Color(0xffd8e6ff)
        val blue = Color(0xff76a9ff)
        Text(
            text = "Caption",
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 4.dp),
            textAlign = TextAlign.Start,
            color = blue
        )
        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = textState.value,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = lightBlue,
                cursorColor = Color.Black,
                disabledLabelColor = lightBlue,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent
            ),
            onValueChange = {
                if (it.length <= maxLength) textState.value = it
            },
            shape = RoundedCornerShape(8.dp),
            singleLine = true,
            trailingIcon = {
                if (textState.value.isNotEmpty()) {
                    IconButton(onClick = { textState.value = "" }) {
                        Icon(
                            imageVector = Icons.Outlined.Close,
                            contentDescription = null
                        )
                    }
                }
            }
        )
        Text(
            text = "${textState.value.length} / $maxLength",
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 4.dp),
            textAlign = TextAlign.End,
            color = blue
        )
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun CustomBasicTextField(){
    var text = remember {
        mutableStateOf("")
    }
    val interactionSource = remember { MutableInteractionSource() }
    val lightBlue = Color(0xffd8e6ff)
    val blue = Color(0xff76a9ff)
    Column(modifier = Modifier.padding(5.dp)) {
        BasicTextField(value = text.value,
            onValueChange = {
                text.value = it
            },
            modifier = Modifier
                .fillMaxWidth()
                .background(color = Color.Transparent, shape = RoundedCornerShape(5.dp))
                .height(30.dp),
            textStyle = LocalTextStyle.current.copy(
                color = Color.Black,
                fontSize = 14.sp
            ),
            singleLine = true
        ){ innerTextField ->
            TextFieldDefaults.TextFieldDecorationBox(
                value = text.value,
                innerTextField = innerTextField,
                visualTransformation = VisualTransformation.None,
                trailingIcon = {
                    if (text.value.isNotEmpty()) {
                        IconButton(onClick = { text.value = "" }) {
                            Icon(
                                imageVector = Icons.Outlined.Close,
                                contentDescription = null
                            )
                        }
                    }
                },
                placeholder = {
                    Text(
                        text = "Search",
                        fontSize = 14.sp,
                    )
                },
                interactionSource = interactionSource,
                enabled = true,
                singleLine = true,
                // keep horizontal paddings but change the vertical
                contentPadding = TextFieldDefaults.textFieldWithoutLabelPadding(
                    top = 0.dp, bottom = 0.dp, start = 5.dp, end = 0.dp
                )
            )
//            innerTextField
        }
//        Divider(color = Color.Black, thickness = 2.dp, modifier = Modifier.height(2.dp))
    }

}

@Preview(showBackground = true)
@Composable
fun DefaultPreview6() {
    NavAppSampleTheme {
        CustomBasicTextField()
    }
}
