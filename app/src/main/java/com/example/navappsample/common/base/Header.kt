package com.example.navappsample.common.base

import android.util.Log
import androidx.activity.ComponentActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.navappsample.R

@Composable
fun MyHeader(title:String?="",
                            isDeleteMode: MutableState<Boolean> = mutableStateOf(false),
                            onBackPressDeleteMode:(() -> Unit)? = null
){
    val context = LocalContext.current
    Column(modifier = Modifier.fillMaxWidth()) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .requiredHeight(50.dp)
            .background(Color.Transparent)
            .padding(start = 10.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Box(modifier = Modifier.fillMaxSize()) {
                IconButton(
                    onClick = {
                        Log.d("BAO", "----> click icon back")
                        if (isDeleteMode.value){
                            isDeleteMode.value = false
                            onBackPressDeleteMode?.invoke()
                        }else{
                            (context as ComponentActivity).finish()
                        }
                    },
                    modifier = Modifier
                        .width(30.dp)
                        .height(30.dp)
                        .align(Alignment.CenterStart)
                ) {
                    Icon(
                        painter = painterResource(R.drawable.ic_back),

                        contentDescription = null,

                        )
                }

                Text(
                    text = title ?: "",
                    modifier = Modifier
                        .align(Alignment.Center),
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Bold
                )

                if (isDeleteMode.value){
                    IconButton(
                        onClick = {
                            Log.d("BAO", "----> click icon delete")

                        },
                        modifier = Modifier
                            .width(30.dp)
                            .height(30.dp)
                            .align(Alignment.CenterEnd)
                    ) {
                        Icon(
                            imageVector = Icons.Default.Delete,

                            contentDescription = null,

                            )
                    }
                }
            }
        }

        Divider(color = Color.LightGray, thickness = 1.dp)
    }
}