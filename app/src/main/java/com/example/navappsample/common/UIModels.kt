package com.example.navappsample.common

import androidx.compose.ui.graphics.vector.ImageVector

data class NavItemModel(
    val title:String,
    val icon:ImageVector,
    val route:String
)