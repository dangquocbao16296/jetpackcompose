package com.example.navappsample.common.base

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.unit.dp

@Composable
fun MyButton(
    modifier: Modifier = Modifier,
    content: @Composable RowScope.() -> Unit
){
    Button(
        onClick = {},
        modifier = modifier
            .defaultMinSize(minHeight = 40.dp)
            .padding(start = 5.dp, end = 5.dp),
       content = content,
        colors = ButtonDefaults.buttonColors(backgroundColor = Color.Blue),
        contentPadding = PaddingValues(start = 5.dp, end = 5.dp
            , top = 0.dp, bottom = 0.dp,
        ),
        shape = RoundedCornerShape(8.dp)
    )
}

@Composable
fun BorderStrolkeButton(
    modifier: Modifier = Modifier,
    onClick:() -> Unit = {},
    content: @Composable RowScope.() -> Unit
){
    Button(
        onClick = {},
        modifier = modifier
            .defaultMinSize(minHeight = 40.dp),
        content = content,
        colors = ButtonDefaults.buttonColors(backgroundColor = Color.White),
        contentPadding = PaddingValues(start = 5.dp, end = 5.dp
            , top = 0.dp, bottom = 0.dp,
        ),
        border = BorderStroke(2.dp, Color.Green),
        shape = RoundedCornerShape(8.dp)
    )
}