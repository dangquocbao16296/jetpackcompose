package com.example.navappsample.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.navappsample.ui.loadmorelist.DynamicListViewModel
import com.example.navappsample.ui.navigationview.NavigationBarViewModel
import com.example.navappsample.ui.tabandlist.TabAndListViewModel

class ViewModelFactory : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        with(modelClass) {
            when {
                isAssignableFrom(TabAndListViewModel::class.java) ->
                    TabAndListViewModel()
                isAssignableFrom(DynamicListViewModel::class.java) ->
                    DynamicListViewModel()
                isAssignableFrom(NavigationBarViewModel::class.java) ->
                    NavigationBarViewModel()
                else -> throw IllegalArgumentException("unknown model class $modelClass")
            }
        } as T
}